# Slovenian translation for ubuntu-calculator-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calculator-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calculator-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-28 13:08+0000\n"
"PO-Revision-Date: 2023-01-04 15:29+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Slovenian <https://hosted.weblate.org/projects/lomiri/"
"lomiri-calculator-app/sl/>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || "
"n%100==4 ? 2 : 3;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:42+0000\n"

#: lomiri-calculator-app.desktop.in:3 app/lomiri-calculator-app.qml:285
msgid "Calculator"
msgstr "Računalo"

#: lomiri-calculator-app.desktop.in:4
#, fuzzy
#| msgid "A calculator for Ubuntu."
msgid "A calculator for Lomiri."
msgstr "Računalo za Ubuntu"

#: lomiri-calculator-app.desktop.in:5
msgid "math;addition;subtraction;multiplication;division;"
msgstr "matematika;seštevanje;odštevanje;množenje;deljenje;"

#: lomiri-calculator-app.desktop.in:7
#, fuzzy
msgid "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"
msgstr "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"

#: app/engine/formula.js:179
msgid "NaN"
msgstr "NaN"

#: app/lomiri-calculator-app.qml:294
msgid "Cancel"
msgstr "Prekliči"

#: app/lomiri-calculator-app.qml:306
msgid "Select All"
msgstr "Izberi vse"

#: app/lomiri-calculator-app.qml:306
msgid "Select None"
msgstr "Odznači vse"

#: app/lomiri-calculator-app.qml:313 app/lomiri-calculator-app.qml:429
msgid "Copy"
msgstr "Kopiraj"

#: app/lomiri-calculator-app.qml:321 app/lomiri-calculator-app.qml:416
msgid "Delete"
msgstr "Izbriši"

#: app/lomiri-calculator-app.qml:439
msgid "Edit"
msgstr "Uredi"

#: app/lomiri-calculator-app.qml:452
#, fuzzy
#| msgid "Edit formula"
msgid "Edit Result"
msgstr "Uredi formulo"

#: app/lomiri-calculator-app.qml:823
#, fuzzy
msgid "Functions"
msgstr "Functions"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: app/ui/BottomEdgePage.qml:55 app/ui/LandscapeKeyboard.qml:39
msgid "log"
msgstr "dnevnik"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: app/ui/BottomEdgePage.qml:59 app/ui/LandscapeKeyboard.qml:46
msgid "mod"
msgstr "mod"

#~ msgid "Favorite"
#~ msgstr "Priljubljeno"

#~ msgid "Add to favorites"
#~ msgstr "Dodaj med priljubljene"

#~ msgid "dd MMM yyyy"
#~ msgstr "d. MMM yyyy"

#~ msgid "No favorites"
#~ msgstr "Ni priljubljenih"

#~ msgid ""
#~ "Swipe calculations to the left\n"
#~ "to mark as favorites"
#~ msgstr ""
#~ "Potegnite izračune na levo\n"
#~ "za označitev kot priljubljene"

#~ msgid "Just now"
#~ msgstr "Pravkar"

#~ msgid "Today "
#~ msgstr "Danes "

#~ msgid "hh:mm"
#~ msgstr "h:mm"

#~ msgid "Yesterday"
#~ msgstr "Včeraj"

#~ msgid "Skip"
#~ msgstr "Preskoči"

#~ msgid "Welcome to Calculator"
#~ msgstr "Dobrodošli v Računalu"

#~ msgid "Enjoy the power of math by using Calculator"
#~ msgstr "Uživajte v moči računanja z Računalom"

#~ msgid "Copy formula"
#~ msgstr "Kopiraj formulo"

#~ msgid "Long press to copy part or all of a formula to the clipboard"
#~ msgstr "Pridržite za kopiranje delne ali celotne formule na odložišče"

#~ msgid "Enjoy"
#~ msgstr "Uživajte"

#~ msgid "We hope you enjoy using Calculator!"
#~ msgstr "Upamo, da uživate pri uporabi Računala!"

#~ msgid "Finish"
#~ msgstr "Končaj"

#~ msgid "Scientific keyboard"
#~ msgstr "Strokovna tipkovnica"

#~ msgid "Access scientific functions with a left swipe on the numeric keypad"
#~ msgstr ""
#~ "Do strokovnih funkcij dostopajte z levim potegom na številčni tipkovnici"

#~ msgid "Scientific View"
#~ msgstr "Strokovni pogled"

#~ msgid "Rotate device to show numeric and scientific functions together"
#~ msgstr ""
#~ "Zasukajte napravo za hkraten prikaz številčnih in strokovnih funkcij"

#~ msgid "Delete item from calculation history"
#~ msgstr "Izbriši iz zgodovine računanja"

#~ msgid "Swipe right to delete items from calculator history"
#~ msgstr "Potegnite na desno za izbris iz zgodovine računanja"

#~ msgid "Delete several items from calculation history"
#~ msgstr "Izbriši nekaj vnosov v zgodovini računanja"

#~ msgid "Long press to select multiple calculations for deletion"
#~ msgstr "Dolgo pritisnite za izbiro več računanj za izbris"

#~ msgid "Delete whole formula at once"
#~ msgstr "Izbriši vso formulo naenkrat"

#~ msgid "Long press '←' button to clear all formulas from input bar"
#~ msgstr "Pridržite gumb '←' za izbris vseh formul v vnosni vrstici"

#~ msgid "Edit item from calculation history"
#~ msgstr "Uredi vnos v zgodovini računanja"

#~ msgid "Swipe left and press pencil to edit calculation"
#~ msgstr "Potegnite levo in pritisnite svinčnik za urejanje računa"

#~ msgid "Add new favourite"
#~ msgstr "Dodaj novo priljubljeno"

#~ msgid "Swipe left and press star to add calculations to favourites view"
#~ msgstr ""
#~ "Potegnite levo in pritisnite zvezdo za dodajanje računov v pogled "
#~ "priljubljenih"

#~ msgid "Click in the middle of a formula to edit in place"
#~ msgstr "Kliknite na sredini formule za urejanje na mestu"
