# Welsh translation for ubuntu-calculator-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calculator-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calculator-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-28 13:08+0000\n"
"PO-Revision-Date: 2023-01-04 15:29+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Welsh <https://hosted.weblate.org/projects/lomiri/"
"lomiri-calculator-app/cy/>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=(n==0) ? 0 : (n==1) ? 1 : (n==2) ? 2 : "
"(n==3) ? 3 :(n==6) ? 4 : 5;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:42+0000\n"

#: lomiri-calculator-app.desktop.in:3 app/lomiri-calculator-app.qml:285
msgid "Calculator"
msgstr "Cyfrifiannell"

#: lomiri-calculator-app.desktop.in:4
#, fuzzy
#| msgid "A calculator for Ubuntu."
msgid "A calculator for Lomiri."
msgstr "Cyfrifiannell ar gyfer Ubuntu"

#: lomiri-calculator-app.desktop.in:5
msgid "math;addition;subtraction;multiplication;division;"
msgstr "math;adio;tynnu;lluosi;rhannu;"

#: lomiri-calculator-app.desktop.in:7
#, fuzzy
msgid "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"
msgstr "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"

#: app/engine/formula.js:179
msgid "NaN"
msgstr "NaN"

#: app/lomiri-calculator-app.qml:294
msgid "Cancel"
msgstr "Diddymu"

#: app/lomiri-calculator-app.qml:306
msgid "Select All"
msgstr "Dewis y Cyfan"

#: app/lomiri-calculator-app.qml:306
msgid "Select None"
msgstr "Dewis Dim"

#: app/lomiri-calculator-app.qml:313 app/lomiri-calculator-app.qml:429
msgid "Copy"
msgstr "Copïo"

#: app/lomiri-calculator-app.qml:321 app/lomiri-calculator-app.qml:416
msgid "Delete"
msgstr "Dileu"

#: app/lomiri-calculator-app.qml:439
msgid "Edit"
msgstr "Golygu"

#: app/lomiri-calculator-app.qml:452
#, fuzzy
msgid "Edit Result"
msgstr "Edit Result"

#: app/lomiri-calculator-app.qml:823
#, fuzzy
msgid "Functions"
msgstr "Functions"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: app/ui/BottomEdgePage.qml:55 app/ui/LandscapeKeyboard.qml:39
msgid "log"
msgstr "log"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: app/ui/BottomEdgePage.qml:59 app/ui/LandscapeKeyboard.qml:46
msgid "mod"
msgstr "mod"

#~ msgid "Favorite"
#~ msgstr "Ffefrynnu"

#~ msgid "Add to favorites"
#~ msgstr "Ychwanegu at ffefrynnau"

#~ msgid "dd MMM yyyy"
#~ msgstr "dd MMM yyyy"

#~ msgid "No favorites"
#~ msgstr "Dim ffefrynnau"

#~ msgid ""
#~ "Swipe calculations to the left\n"
#~ "to mark as favorites"
#~ msgstr ""
#~ "Trawio cyfrifiadau i'r chwith\n"
#~ "i'w nodi fel ffefrynnau"

#~ msgid "Just now"
#~ msgstr "Y funud hon"

#~ msgid "Today "
#~ msgstr "Heddiw "

#~ msgid "hh:mm"
#~ msgstr "hh:mm"

#~ msgid "Yesterday"
#~ msgstr "Ddoe"
