# Math.js

Running math.js under QML requires a custom build that is only 3 characters
different from a real build. For some reason qml does not like the custom
errors that math.js uses and breaks. But if you replace `"use strict";` with
`;"use strict";` it magically works. Thanks to the tip on this
[GitHub issue](https://github.com/josdejong/mathjs/issues/1454).
